#!/usr/bin/env python

"""
Dumper for VR track jets that are reconstructed in trigger
"""

import sys
from FTagDumper import dumper, mctc
from FTagDumper.trigger import getLabelingBuilderAlg, getJobConfig
from JetTagCalibration.JetTagCalibConfig import JetTagCalibCfg
from BTagging.BTagConfig import BTagAlgsCfg
from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from ParticleJetTools.ParticleJetToolsConfig import (
    getJetDeltaRFlavorLabelTool)


def getRequisiteAlgsCA(cfgFlags):
    fs_tracks = 'HLT_IDTrack_FS_FTF'
    fs_vertices = 'HLT_IDVertex_FS'
    ca = ComponentAccumulator()
    ca.merge(mctc.getMCTC())
    labelAlg = getLabelingBuilderAlg(cfgFlags)
    ca.addEventAlgo(labelAlg)
    DRTool = getJetDeltaRFlavorLabelTool()
    ca.addEventAlgo(CompFactory.JetDecorationAlg(
        "HLT_TrackJetDeltaRLabelingAlg",
        Decorators = [DRTool],
        JetContainer = "HLT_AntiKtVR30Rmax4Rmin02PV0TrackJets"
    ))
    ca.addEventAlgo(CompFactory.FlavorTagDiscriminants.PoorMansIpAugmenterAlg(
        name = "PoorMansIpAugmenter_HLT_IDTrack_FS_FTF",
        prefix = 'btagIp_',
        trackContainer = fs_tracks,
        primaryVertexContainer = fs_vertices))

    OverlapDecoTool = CompFactory.TriggerVRJetOverlapDecoratorTool()
    ca.addEventAlgo(CompFactory.JetDecorationAlg(
            "HLT_VRJetOverlapDecoratorAlg",
            Decorators = [OverlapDecoTool],
            JetContainer = "HLT_AntiKtVR30Rmax4Rmin02PV0TrackJets"))
    ca.merge(JetTagCalibCfg(cfgFlags))
    ca.merge(BTagAlgsCfg(
        cfgFlags,
        JetCollection="HLT_AntiKtVR30Rmax4Rmin02PV0TrackJets",
        nnList=['BTagging/20211216trig/dips/AntiKt4EMPFlow/network.json'],
        trackCollection= fs_tracks,
        primaryVertices= fs_vertices,
        muons='',
        renameTrackJets = False,
        AddedJetSuffix=''
    ))
    return ca

def run():

    args = dumper.base_parser(__doc__).parse_args()

    from AthenaConfiguration.AllConfigFlags import ConfigFlags as cfgFlags
    dumper.update_cfgFlags(cfgFlags, args)
    cfgFlags.lock()

    # load configurations
    combined_cfg = dumper.combinedConfig(args.config_file)
    trig_cfg, dumper_cfg = getJobConfig(combined_cfg)

    top_level_ca = dumper.getMainConfig(cfgFlags, args)
    top_level_ca.merge(getRequisiteAlgsCA(cfgFlags))
    top_level_ca.merge(dumper.getDumperConfig(args, config_dict=dumper_cfg))
    return top_level_ca.run()

if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)
