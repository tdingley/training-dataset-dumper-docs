from dataclasses import dataclass

from AthenaServices.MetaDataSvcConfig import MetaDataSvcCfg
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

from .BaseBlock import BaseBlock

@dataclass
class GeneratorWeights(BaseBlock):
    """
    Add truth weights to EventInfo
    """
    def to_ca(self):
        return PMGTruthWeightAlgCfg(self.cfgFlags)


def PMGTruthWeightAlgCfg(flags):
    ca = ComponentAccumulator()
    # we need both the systematics service and the metadata service to
    # make tis tool work.
    ca.addService(
        CompFactory.CP.SystematicsSvc(
            name="SystematicsSvc",
            sigmaRecommended=1,
            systematicsRegex='.*',
        )
    )
    ca.merge(MetaDataSvcCfg(flags))
    ca.addEventAlgo(
        CompFactory.CP.PMGTruthWeightAlg(
            name="PMGTruthWeightAlg",
            truthWeightTool=CompFactory.PMGTools.PMGTruthWeightTool(
                name="PMGTruthWeightTool"
            ),
            decoration = 'generatorWeight_%SYS%',
        )
    )
    return ca

